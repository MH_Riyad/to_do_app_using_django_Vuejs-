from django.urls import path,include

from .views import TaskListCreateAPIView
app_name = 'task'
urlpatterns = [
    path('', TaskListCreateAPIView.as_view(), name='TaskListCreateAPIView')
]
